import React, { useState } from 'react';
import { TextField, Button, Stack, Grid, CircularProgress } from '@mui/material';
import axios from 'axios';
import moment from 'moment';

const API_KEY = "RGAPI-9f41d1d6-c7de-402f-b347-eaf72b8df44c"

const DATE_NOW = Date.now();

const SERVER_TO_REGION = {
  "euw1": { region: "europe", name: "EUW" },
  "eun1": { region: "europe", name: "EUNE" },
  "ru": { region: "europe", name: "RU" },
  "tr1": { region: "europe", name: "TR" },
  "kr": { region: "asia", name: "KR" },
  "jp1": { region: "asia", name: "JP" },
  "br1": { region: "americas", name: "BR" },
  "la1": { region: "americas", name: "LAN" },
  "la2": { region: "americas", name: "LAS" },
  "na1": { region: "americas", name: "NA" },
  "oc1": { region: "sea", name: "OCE" },
};

const DAYS_SELECTION = {
  "today": { name: "last 24 hours", value: DATE_NOW - 1 * 24 * 60 * 60 * 1000 },
  "3days": { name: "3 days", value: DATE_NOW - 3 * 24 * 60 * 60 * 1000 },
  "15days": { name: "15 days", value: DATE_NOW - 15 * 24 * 60 * 60 * 1000 },
  "1month": { name: "1 month", value: DATE_NOW - 30 * 24 * 60 * 60 * 1000 },
  "3months": { name: "3 months", value: DATE_NOW - 90 * 24 * 60 * 60 * 1000 },
  "6months": { name: "6 months", value: DATE_NOW - 180 * 24 * 60 * 60 * 1000 },
  "1year": { name: "1 year", value: DATE_NOW - 365 * 24 * 60 * 60 * 1000 },
}

export default function Home() {
  const [valeur, setValeur] = useState('');
  const [selectedRegion, setSelectedRegion] = useState('euw1');
  const [selectedButton, setSelectedButton] = useState('euw1');
  const [selectedHowManyDays, setSelectedHowManyDays] = useState('today');
  const [selectedButtonHowManyDays, setSelectedButtonHowManyDays] = useState('today');
  const [totalDuration, setTotalDuration] = useState(0);
  const [isLoading, setIsLoading] = useState(true);

  const handleRegion = (e) => {
    setSelectedRegion(e)
    setSelectedButton(e);
  };

  const handleDaysSelected = (e) => {
    setSelectedHowManyDays(e.target.value)
    setSelectedButtonHowManyDays(e.target.value);
  };

  const handleChange = (event) => {
    setValeur(event.target.value);
  };

  const getRegion = (server) => SERVER_TO_REGION[server]?.region;

  const generateButtonsForRegions = () => {
    const buttons = Object.entries(SERVER_TO_REGION).map(([server, data]) => (
      <Button
        key={server}
        variant="contained"
        onClick={() => handleRegion(server)}
        value={server}
        style={{ backgroundColor: selectedButton === server ? 'green' : '#1876D2' }}
      >
        {data.name}
      </Button>
    ));
    return buttons;
  };

  const generateDayButtons = () => {
    return Object.keys(DAYS_SELECTION).map((key) => (
      <Button
        key={key}
        variant="contained"
        value={key}
        onClick={handleDaysSelected}
        style={{
          backgroundColor: selectedButtonHowManyDays === key ? 'green' : '#1876D2'
        }}>
        {DAYS_SELECTION[key].name}
      </Button>
    ));
  }

  const fetchMatchData = async (matchIds) => {
    let totalDuration = moment.duration(0);
    for (const matchId of matchIds) {
      try {
        const response = await axios.get(`https://europe.api.riotgames.com/lol/match/v5/matches/${matchId}?api_key=${API_KEY}`);
        const gameDuration = moment.duration(response.data.info.gameDuration, 'seconds');;
        totalDuration.add(gameDuration);
      } catch (error) {
        console.error(error);
      }
    }
    setIsLoading(false);
    let totalDurationFormatted = '';
    if (totalDuration.asDays() >= 365) {
      totalDurationFormatted = `${totalDuration.asYears().toFixed(1)} years`;
    } else if (totalDuration.asDays() >= 30) {
      totalDurationFormatted = `${totalDuration.asMonths().toFixed(1)} months`;
    } else if (totalDuration.asDays() >= 1) {
      totalDurationFormatted = `${totalDuration.asDays().toFixed(1)} days`;
    } else if (totalDuration.asHours() >= 1) {
      totalDurationFormatted = `${totalDuration.asHours().toFixed(1)} hours`;
    } else if (totalDuration.asMinutes() >= 1) {
      totalDurationFormatted = `${totalDuration.asMinutes().toFixed(1)} minutes`;
    } else {
      totalDurationFormatted = `${totalDuration.asSeconds()} seconds`;
    }

    setTotalDuration(totalDurationFormatted);
  };


  const handleSubmit = (event) => {
    let region = getRegion(selectedRegion);
    let TimeNowInEpoch = Math.trunc(DATE_NOW / 1000)
    let TimeSelectedInEpoche = Math.trunc(DAYS_SELECTION[selectedHowManyDays].value / 1000)
    event.preventDefault();
    axios.defaults.withCredentials = false
    axios
      .get(`https://${selectedRegion}.api.riotgames.com/lol/summoner/v4/summoners/by-name/${valeur}?api_key=${API_KEY}`)
      .then((response) => {
        axios.get(`https://${region}.api.riotgames.com/lol/match/v5/matches/by-puuid/${response.data.puuid}/ids?startTime=${TimeSelectedInEpoche}&endTime=${TimeNowInEpoch}&start=0&count=100&api_key=${API_KEY}`)
          .then(response => {
            let matchIds = response.data;
            console.log(matchIds)
            fetchMatchData(matchIds);
          })
          .catch(error => console.error(error));
      })
      .catch((error) => console.error(error));
  };

  return (
    <form onSubmit={handleSubmit}>
      <Grid>
        <Stack alignItems="center" p={3} spacing={2}>
          <TextField
            sx={{ width: 500 }}
            label="Saisissez votre pseudo d'invocateur"
            variant="outlined"
            value={valeur}
            onChange={handleChange}
          />

          <Stack spacing={1} textAlign="center" alignItems="center">
            <Stack direction="row" spacing={1}>
              {generateButtonsForRegions()}
            </Stack>
            <Stack direction="row" spacing={1} >
              {generateDayButtons()}
            </Stack>
            <Stack>
              <p> ton temps passer en game bouffon </p>
              {!isLoading ? <p>{totalDuration}</p> : <CircularProgress />}
            </Stack>
          </Stack>
          <Button variant="contained" color="primary" type="submit">
            Envoyer
          </Button>
        </Stack>
      </Grid>
    </form>
  );
}

