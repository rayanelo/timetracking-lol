const corsProxy = require('cors-anywhere');

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 8080;

corsProxy.createServer({
  originWhitelist: [], // permet toutes les requêtes
  requireHeader: ['origin', 'x-requested-with'],
  removeHeaders: ['cookie', 'cookie2']
}).listen(port, host, () => {
  console.log(`Proxy CORS-anywhere démarré sur ${host}:${port}`);
});
