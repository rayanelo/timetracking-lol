import requests

# Définir votre clé d'API Riot Games
api_key = "RGAPI-185a3511-8bc9-4d33-8cc1-fdfed2238f72"

# Définir le nom d'utilisateur et la région du joueur que vous voulez rechercher
summoner_name = "paxpax"
region = "euw1"

# Envoyer une requête API pour récupérer l'ID d'invocateur du joueur
response = requests.get(f"https://{region}.api.riotgames.com/lol/summoner/v4/summoners/by-name/{summoner_name}?api_key={api_key}")
puuid = response.json()["puuid"]

# Envoyer une autre requête API pour récupérer l'historique de parties du joueur
response = requests.get(f"https://europe.api.riotgames.com/lol/match/v5/matches/by-puuid/{puuid}/ids?start=0&count=20&api_key={api_key}")
matches = response.json()

# Récupération des ID de chaque game et stockage dans un tableau
match_ids = []
for match in matches:
    match_ids.append(match)

# Récupération de la durée en seconde de chaque game et convertion en minutes
def get_game_durations(matches, api_key):
    durations = []
    for match in matches:
        response = requests.get(f"https://europe.api.riotgames.com/lol/match/v5/matches/{match}?api_key={api_key}")
        duration_in_seconds = response.json()["info"]["gameDuration"]
        duration_in_minutes = round(duration_in_seconds / 60)
        durations.append(duration_in_minutes)
    return durations

# Appeler la fonction pour récupérer les durées de jeu et stocker les résultats dans une variable
durations = get_game_durations(match_ids, api_key)

# Calculer la durée totale de jeu
total_duration = sum(durations)

# Calculer la durée moyenne de jeu par jour sur une semaine
average_duration_per_day = (total_duration / 60) / 7

# Afficher la durée moyenne de jeu par jour sur une semaine
print(f"Temps passé pour les 20 dernières games : {total_duration} minutes ({total_duration // 60} heures)")
print(f"Vous  jouez environ : {average_duration_per_day:.2f} heures par jour.")